package com.shankar.naksa.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.shankar.naksa.Model.Employee;
import com.shankar.naksa.R;

import java.util.ArrayList;
import java.util.List;

public class EmployeeListAdapter extends RecyclerView.Adapter<EmployeeListAdapter.ViewHolder> {

    public interface ItemClickListener {
        void onclick();
    }

    private ArrayList<Employee> mDataset;
    Context context;
    private ItemClickListener clickListener;


    // Provide a suitable constructor (depends on the kind of dataset)
    public EmployeeListAdapter(ArrayList<Employee> myDataset,Context context) {
        this.mDataset = myDataset;
        this.context=context;

    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }




    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        // each data item is just a string in this case
        public TextView name, position, contact;

        public ViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            position = (TextView) view.findViewById(R.id.position);
            contact = (TextView) view.findViewById(R.id.phone_number);
            itemView.setTag(itemView);
            itemView.setOnClickListener(this);


        }


        public void bind(final Employee employee) {
            name.setText(employee.getName());
            position.setText(employee.getPosition());
            contact.setText(employee.getContact());


        }

        @Override
        public void onClick(View v) {
            if (clickListener != null) clickListener.onclick();
        }
    }


    // Create new views (invoked by the layout manager)
    @Override
    public EmployeeListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                             int viewType) {
        // create a new view
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.employee_list_row, parent, false);

        ViewHolder vh = new ViewHolder(itemView);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.bind(mDataset.get(position));



    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
