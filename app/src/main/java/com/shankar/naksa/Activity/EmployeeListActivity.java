package com.shankar.naksa.Activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.shankar.naksa.Adapter.EmployeeListAdapter;
import com.shankar.naksa.Model.Employee;
import com.shankar.naksa.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EmployeeListActivity extends AppCompatActivity  implements  EmployeeListAdapter.ItemClickListener{

    @BindView(R.id.my_recycler_view)
    RecyclerView mRecyclerView;


     EmployeeListAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    ArrayList<Employee> employeeArrayList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_list);
        ButterKnife.bind(this);
        employeeArrayList = getEmployees();

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        // specify an adapter (see also next example)
        mAdapter = new EmployeeListAdapter(employeeArrayList,this);



        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setClickListener(this);


    }

    public ArrayList<Employee> getEmployees(){

        ArrayList<Employee> listOfEmployees=  new ArrayList<>();


        Employee employee = new Employee();
        employee.setName("Uday Bahadur Ranamagar");
        employee.setPosition("Chief District Officer");
        employee.setContact("9857077777");
        listOfEmployees.add(employee);


        employee = new Employee();
        employee.setName("Narayan Pandeya");
        employee.setPosition("Assistant District Officer");
        employee.setContact("9857035111");
        listOfEmployees.add(employee);

        employee = new Employee();
        employee.setName("Ratneshwor Gubhaju");
        employee.setPosition("Administrative Officer");
        employee.setContact("9847020682");
        listOfEmployees.add(employee);


        Employee employee4 = new Employee();
        employee4.setName("Toya Narayan Aryal");
        employee4.setPosition("Administrative Officer");
        employee4.setContact("9747012180");
        listOfEmployees.add(employee4);

        employee = new Employee();
        employee.setName("Bishnu Parshad Gnawali");
        employee.setPosition("Administrative Officer");
        employee.setContact("9857030888");
        listOfEmployees.add(employee);

        employee = new Employee();
        employee.setName("Bishnu Bahadur Gharti");
        employee.setPosition("Administrative Officer");
        employee.setContact("9857064444");
        listOfEmployees.add(employee);


        employee = new Employee();
        employee.setName("Lunja lal Shakya");
        employee.setPosition("Accounting Officer");
        employee.setContact("9857035430");
        listOfEmployees.add(employee);

        employee = new Employee();
        employee.setName("Yagya Raj Pokhrel");
        employee.setPosition("Nayab Subbha");
        employee.setContact("9857029336");
        listOfEmployees.add(employee);

        employee = new Employee();
        employee.setName("Salikram Bhandari");
        employee.setPosition("Nayab Subbha");
        employee.setContact("9847087433");
        listOfEmployees.add(employee);

        employee = new Employee();
        employee.setName("Pitambar Khanal");
        employee.setPosition("Nayab Subbha");
        employee.setContact("9847028085");
        listOfEmployees.add(employee);


        employee = new Employee();
        employee.setName("Madhav Prashad Ghimire");
        employee.setPosition("Nayab Subbha");
        employee.setContact("9847275666");
        listOfEmployees.add(employee);

        employee = new Employee();
        employee.setName("BalKrishna Acharya");
        employee.setPosition("Nayab Subbha");
        employee.setContact("9841526331");
        listOfEmployees.add(employee);

        employee = new Employee();
        employee.setName("Arjun Panthi");
        employee.setPosition("Computer Operator");
        employee.setContact("9847151113");
        listOfEmployees.add(employee);


        employee = new Employee();
        employee.setName("Dadhu parshad Neupane");
        employee.setPosition("Accountant");
        employee.setContact("9847030304");
        listOfEmployees.add(employee);

        employee = new Employee();
        employee.setName("Raj Kumar Yadav");
        employee.setPosition("Kharidar");
        employee.setContact("9847058845");
        listOfEmployees.add(employee);



        employee4.setName("Amrita Neupane");
        employee4.setPosition("Kharidar");
        employee4.setContact("9867022956");
        listOfEmployees.add(employee4);

        employee = new Employee();
        employee.setName("Mina Kunwar");
        employee.setPosition("Kharidar");
        employee.setContact("9847441209");
        listOfEmployees.add(employee);

        employee = new Employee();
        employee.setName("Tek Naryan Neupane");
        employee.setPosition("Kharidar");
        employee.setContact("9847216528");
        listOfEmployees.add(employee);


        employee = new Employee();
        employee.setName("Mitralal Dhakal");
        employee.setPosition("Assistant Computer Operator");
        employee.setContact("9851193965");
        listOfEmployees.add(employee);

        employee = new Employee();
        employee.setName("Yubaraj Ghimire");
        employee.setPosition("Assistant Computer Operator");
        employee.setContact("9847091466");
        listOfEmployees.add(employee);

        employee = new Employee();
        employee.setName("Kalpana Kumari Rokaya");
        employee.setPosition("Assistant Computer Operator");
        employee.setContact("9848640203");
        listOfEmployees.add(employee);

        employee = new Employee();
        employee.setName("Punaram Prashad Neupane");
        employee.setPosition("Driver");
        employee.setContact("9847017588");
        listOfEmployees.add(employee);


        employee = new Employee();
        employee.setName("Khem Bahadur Gurung");
        employee.setPosition("Driver");
        employee.setContact("9847020391");
        listOfEmployees.add(employee);

        employee = new Employee();
        employee.setName("Hari Prashad Kafle");
        employee.setPosition("Office Helper");
        employee.setContact("9847039906");
        listOfEmployees.add(employee);

        employee = new Employee();
        employee.setName("Durga Prashad Adhikari");
        employee.setPosition("Office Helper");
        employee.setContact("9847510523");
        listOfEmployees.add(employee);

        employee = new Employee();
        employee.setName("Kamal Kishor Jha");
        employee.setPosition("Office Helper");
        employee.setContact("9804437265");
        listOfEmployees.add(employee);

        employee = new Employee();
        employee.setName("Shambhu Mandal");
        employee.setPosition("Office Helper");
        employee.setContact("9847388136");
        listOfEmployees.add(employee);

        employee = new Employee();
        employee.setName("Jitendra Mahato Nuniya");
        employee.setPosition("Office Helper");
        employee.setContact("9847541996");
        listOfEmployees.add(employee);
        return listOfEmployees;

    }


    @Override
    public void onclick() {
        Toast.makeText(this,"HOJAAA BHAGWAN",Toast.LENGTH_LONG).show();
    }
}
