package com.shankar.naksa.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.github.barteksc.pdfviewer.PDFView;
import com.shankar.naksa.Constant;
import com.shankar.naksa.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CarterCitizenActivity extends AppCompatActivity {

    @BindView(R.id.pdfView)
    PDFView pdfView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_detail);
        ButterKnife.bind(this);

        pdfView.fromAsset(Constant.CARTERCITIZENPDF).load();
    }



}
