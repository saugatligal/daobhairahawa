package com.shankar.naksa.CompoundView;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.shankar.naksa.Constant;
import com.shankar.naksa.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainScreenLayout extends LinearLayout implements View.OnClickListener {


    @BindView(R.id.introduction)
    Button introductionButton;

    @BindView(R.id.list_of_employee)
    Button listOfEmployee;

    @BindView(R.id.gallery)
    Button galleryButton;

    @BindView(R.id.nagarik)
    Button nagarikButton;


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.list_of_employee) {
            mEventListener.onClickevent(Constant.LISTOFEMPLOYEE);
        } else if (v.getId() == R.id.nagarik) {
            mEventListener.onClickevent(Constant.NAGARIK);
        } else if (v.getId() == R.id.introduction) {
            mEventListener.onClickevent(Constant.INTROUCTION);

        } else if (v.getId() == R.id.gallery) {
            mEventListener.onClickevent(Constant.GALLERY);
        }
    }

    public interface OnclickMainScreenButtonListener {
        public void onClickevent(int n);
    }


    private OnclickMainScreenButtonListener mEventListener;

    public void setEventListener(OnclickMainScreenButtonListener mEventListener) {
        this.mEventListener = mEventListener;
    }


    public MainScreenLayout(Context context) {
        super(context);
        initializeViews(context);
    }

    public MainScreenLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initializeViews(context);
    }

    public MainScreenLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initializeViews(context);
    }


    /**
     * Inflates the views in the layout.
     *
     * @param context the current context for the view.
     */
    private void initializeViews(Context context) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.main_screen_layout, this);
        ButterKnife.bind(this);
        introductionButton.setOnClickListener(this);
        listOfEmployee.setOnClickListener(this);
        galleryButton.setOnClickListener(this);
        nagarikButton.setOnClickListener(this);
    }
}
